<?php
    $id = $_POST['idDelete'];

    if($id)
    {
      $conn_string = "host=localhost port=5432 dbname=ejercitario5 user=postgres password=postgres";
      $conexion = pg_connect($conn_string);
      
      $sql = "delete from producto where producto_id = '{$id}'";
      $result = pg_query($conexion, $sql);

      if ($result != null){
        echo "El registro se ha eliminado correctamente";
        header("Refresh: 3;url=mantener-producto.php");
      } else {
        echo "Error al eliminar el registro";
        header("Refresh: 3;url=mantener-producto.php");
      }

    }else{
      echo "Debe enviar el ID del registro a eliminar";
      header("Refresh: 3;url=mantener-producto.php");
    }
?>
